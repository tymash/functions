const getSum = (str1, str2) => {
  if (typeof str1 !== "string" || typeof str2 !== "string") return false;
  if (str1 == "") str1 = 0;
  if (str2 == "") str2 = 0;
  if (!/^\d+$/.test(str1) || !/^\d+$/.test(str2)) return false;

  let sum = parseInt(str1) + parseInt(str2);
  return sum.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  if (Array.isArray(listOfPosts)) {
    let comments = listOfPosts.flatMap(post => post.comments);
    let filteredComments = comments.filter(comment => comment !== undefined && comment.author == authorName);
    let filteredPosts = listOfPosts.filter(post => post.author == authorName);
    const numberOfPosts = filteredPosts.length;
    const numberOfComments = filteredComments.length;
    return `Post:${numberOfPosts},comments:${numberOfComments}`;
  }
  return "Post:0,comments:0`"
};

const tickets = (people) => {
  let changeNotes =
  {
    '25': 0,
    '50': 0,
    '100': 0,
  };

  for (const element of people) {
    if (element === 25) {
      changeNotes['25'] += 1;
    }
    else if (element === 50) {
      if (changeNotes['25'] > 0) {
        changeNotes['50'] += 1;
        changeNotes['25'] -= 1;
      }
      else {
        return 'NO';
      }
    }
    else {
      if (changeNotes['50'] > 0 && changeNotes['25'] > 0) {
        changeNotes['50'] -= 1;
        changeNotes['25'] -= 1;
        changeNotes['100'] += 1;
      }
      else if (changeNotes['25'] >= 3) {
        changeNotes['25'] -= 3;
        changeNotes['100'] += 1;
      }
      else {
        return 'NO';
      }
    }
  }
  return 'YES';
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
